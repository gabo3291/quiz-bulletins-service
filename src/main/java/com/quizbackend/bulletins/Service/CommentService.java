package com.quizbackend.bulletins.Service;

import com.quizbackend.bulletins.Dto.CommentRequest;
import com.quizbackend.bulletins.Entity.Comment;
import com.quizbackend.bulletins.Repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Freddy Perez GUarachi
 */
@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;

    public List<Comment> getCommentsByIdBulletin(Long bulletinId) {
        return commentRepository.findByBulletinId(bulletinId);
    }

    public List<Comment> getCommentsByReplyId(Long commentId) {
        return commentRepository.findByReplyId(commentId);
    }

    public Comment saveComment(CommentRequest commentRequest, Long accountId, Long bulletinId) {
        Comment comment = new Comment(
                accountId,
                bulletinId,
                commentRequest.getSenderUserId(),
                commentRequest.getContent()
            );
        return commentRepository.save(comment);
    }

    public Comment saveReply(Long commentId, CommentRequest commentRequest) {
        Comment comment = new Comment(
                commentId,
                commentRequest.getSenderUserId(),
                commentRequest.getContent()
        );
        return commentRepository.save(comment);
    }
}
