package com.quizbackend.bulletins.Service;

import com.quizbackend.bulletins.Dto.BulletinRequest;
import com.quizbackend.bulletins.Entity.Attachment;
import com.quizbackend.bulletins.Entity.Bulletin;
import com.quizbackend.bulletins.Repository.BulletinRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Freddy Perez GUarachi
 */
@NoArgsConstructor
@Service
public class BulletinService {

    @Autowired
    private BulletinRepository bulletinRepository;

    public List<Bulletin> getAllBulletins() {
        return bulletinRepository.findAll();
    }

    public Bulletin saveBulletin(
            BulletinRequest bulletinRequest,
            String accountId,
            String userId
    ) {
        List<Attachment> attachments = new ArrayList<>();
        List<String> fields = bulletinRequest.getFileIds();

        for (String field : fields) {
            Attachment attachment = new Attachment(Long.parseLong(accountId), field);
            attachments.add(attachment);
        }

        Bulletin bulletin = new Bulletin(
                Long.parseLong(accountId),
                Long.parseLong(userId),
                bulletinRequest.getContent(),
                new Date(),
                attachments
        );
        return bulletinRepository.save(bulletin);
    }
}
