package com.quizbackend.bulletins.Controller;
import com.quizbackend.bulletins.Dto.BulletinRequest;
import com.quizbackend.bulletins.Dto.BulletinResponse;
import com.quizbackend.bulletins.Entity.Bulletin;
import com.quizbackend.bulletins.Service.BulletinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Freddy Perez GUarachi
 */
@RestController
@RequestMapping("api/v1/bulletins")
public class BulletinController {

    @Autowired
    private BulletinService bulletinService;

    @GetMapping
    public List<Bulletin> getAllBulletins() {
        return bulletinService.getAllBulletins();
    }

    @PostMapping
    public ResponseEntity<?> saveBulletin(
            @RequestBody BulletinRequest bulletin,
            @RequestHeader("Account-ID") String accountId,
            @RequestHeader("User-ID") String userId
    ) {
        Bulletin bulletinResponse = bulletinService.saveBulletin(bulletin, accountId, userId);
        return ResponseEntity.ok(
                new BulletinResponse(bulletinResponse.getBody(), bulletinResponse.getAttachments())
        );
    }

}
