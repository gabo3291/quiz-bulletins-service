package com.quizbackend.bulletins.Controller;

import com.quizbackend.bulletins.Dto.CommentRequest;
import com.quizbackend.bulletins.Entity.Comment;
import com.quizbackend.bulletins.Service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Freddy Perez GUarachi
 */
@RestController
@RequestMapping("api/v1/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping("{id}")
    public List<Comment> getCommentsByIdBulletin(@PathVariable Long id) {
        return commentService.getCommentsByIdBulletin(id);
    }

    @GetMapping("{id}/replies")
    public List<Comment> getCommentsByIdComment(@PathVariable Long id) {
        return commentService.getCommentsByReplyId(id);
    }

    @PostMapping
    public Comment saveComment(
            @RequestBody CommentRequest commentRequest,
            @RequestHeader("Account-ID") Long accountId,
            @RequestHeader("Bulletin-ID") Long bulletinId
        ) {
        return commentService.saveComment(commentRequest, accountId, bulletinId);
    }

    @PostMapping("{id}/replies")
    public Comment saveReply(
            @PathVariable Long id,
            @RequestBody CommentRequest commentRequest
    ) {
        return commentService.saveReply(id, commentRequest);
    }


}
