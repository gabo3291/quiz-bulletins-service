package com.quizbackend.bulletins.Dto;

import com.quizbackend.bulletins.Entity.Attachment;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author Freddy Perez GUarachi
 */
@Data
@ToString
public class BulletinResponse {
    private String body;
    private List<Attachment> attachments;

    public BulletinResponse(String body, List<Attachment> attachments) {
        this.body = body;
        this.attachments = attachments;
    }
}
