package com.quizbackend.bulletins.Dto;

/**
 * @author Freddy Perez GUarachi
 */
public class CommentRequest {
    private Long senderUserId;
    private String content;

    public Long getSenderUserId() {
        return senderUserId;
    }

    public String getContent() {
        return content;
    }
}
