package com.quizbackend.bulletins.Dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Freddy Perez GUarachi
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TextSearchRequest {
    private String content;
}
