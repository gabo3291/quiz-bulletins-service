package com.quizbackend.bulletins.Dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * @author Freddy Perez GUarachi
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BulletinRequest {
    private String content;
    private List<String> fileIds;

    public String getContent() {
        return content;
    }

    public List<String> getFileIds() {
        return fileIds;
    }
}
