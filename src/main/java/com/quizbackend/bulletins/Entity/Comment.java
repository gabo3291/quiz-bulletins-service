package com.quizbackend.bulletins.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author Freddy Perez GUarachi
 */
@Data
@NoArgsConstructor
@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long accountId;
    private Long bulletinId;
    private Long replyId;
    private Long senderUserId;
    private String content;
    private Integer repliesCounter = 0;
    private Date createdDate = new Date();
    private Boolean isDeleted = false;

    public Comment(Long accountId, Long bulletinId, Long senderUserId, String content) {
        this.accountId = accountId;
        this.bulletinId = bulletinId;
        this.senderUserId = senderUserId;
        this.content = content;
    }
    public Comment(Long replyId, Long senderUserId, String content) {
        this.replyId = replyId;
        this.senderUserId = senderUserId;
        this.content = content;
    }
}
