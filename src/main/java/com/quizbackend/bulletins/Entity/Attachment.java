package com.quizbackend.bulletins.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author Freddy Perez GUarachi
 */
@Data
@NoArgsConstructor
@Entity(name = "attachments")
public class Attachment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long accountId;
    private String field;
    private Date createdDate = new Date();
    private Boolean isDeleted = false;

    public Attachment(Long accountId, String field) {
        this.accountId = accountId;
        this.field = field;
    }
}
