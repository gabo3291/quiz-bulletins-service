package com.quizbackend.bulletins.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author Freddy Perez GUarachi
 */
@Data
@NoArgsConstructor
@Entity(name = "bulletins")
public class Bulletin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long accountId;
    private Long senderUserId;
    private String body;
    private Date createdDate;
    private Boolean isDeleted = false;
    private Integer commentsCounter = 0;

    @OneToMany(targetEntity = Attachment.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "ba_fk", referencedColumnName = "id")
    private List<Attachment> attachments;

    public Bulletin(Long accountId, Long senderUserId, String body, Date createdDate, List<Attachment> attachments) {
        this.accountId = accountId;
        this.senderUserId = senderUserId;
        this.body = body;
        this.createdDate = createdDate;
        this.attachments = attachments;
    }

    public String getBody() {
        return body;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }
}
