package com.quizbackend.bulletins.Repository;

import com.quizbackend.bulletins.Entity.Bulletin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Freddy Perez GUarachi
 */
@Repository
public interface BulletinRepository extends JpaRepository<Bulletin, Long> {
}
