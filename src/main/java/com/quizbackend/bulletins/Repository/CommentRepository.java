package com.quizbackend.bulletins.Repository;

import com.quizbackend.bulletins.Entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Freddy Perez GUarachi
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findByBulletinId(Long bulletinId);
    List<Comment> findByReplyId(Long commentId);
}
