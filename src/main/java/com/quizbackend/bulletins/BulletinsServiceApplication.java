package com.quizbackend.bulletins;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BulletinsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BulletinsServiceApplication.class, args);
	}

}
